#!/usr/bin/env python

from heapq import nlargest
from math import exp, log, cosh, tanh, sqrt
import matplotlib.pyplot as plt
from modred import DMD
import numpy as np
from numpy import arange, dot, vdot, loadtxt, reshape, argmin
from os import listdir
from os.path import isfile, isdir, join
import matplotlib.cm as cm
import mpl_toolkits.mplot3d.axes3d as p3
import pylab as p


data_dir = "data/"
sim_dir = "simulation/"
N = 20
sigma = 1 #noise
sigma2 = sigma*sigma

mult = 1e-6
num_modes = 5
data = []


# most unstable dmd modes->sum of squares
def compute_field_info(directory):
  # read files
  vecs = []
  files = [ f for f in listdir(directory) if isfile(join(directory,f)) ]
  files.sort()
  for f in files:
    x,y,val = loadtxt(directory+"/"+f).T #Transposed for easier unpacking
    vecs.append(val)
  # compute dmd
  myDMD = DMD(vdot)
  ritz_values, modes_norms, build_coeffs = myDMD.compute_decomp_in_memory(vecs)
  real = [k for k in ritz_values.real]
  largest_modes = sorted(range(len(real)), key=lambda i:real[i])
  modes = myDMD.compute_modes_in_memory(largest_modes[0:num_modes])
  # compute norm
  s = 0
  for i in range(0,num_modes):
    m = max(modes[i])
    for j in range(0,len(modes[i])):
      # normalize modes
      modes[i][j] /= m
      z = modes[i][j]
      s += z.real*z.real+z.imag*z.imag;
  s = sqrt(s)
  return s


def log_likelihood(curr_dir):
  finfo = compute_field_info(curr_dir)
  s = 0
  for k in range(0,N):
    t = data[k] - finfo
    s += t*t
  return 0.5*s/sigma2


# compute data
for i in range(0,N):
  s = compute_field_info(data_dir+str(i))
  data.append(s)
#print "data: ", data

# compute posterior = compute log likelihood
f = open("posterior.txt", "w")
dirs = [ d for d in listdir(sim_dir) if isdir(join(sim_dir,d)) ]
dirs.sort()
val = []
a = []
b = []
for d in dirs:
  curr_val = log_likelihood(sim_dir+d)*mult
  name = d.replace("_", " ").split()
  curr_a = float(name[1])
  curr_b = float(name[2])
  f.write("%lf %lf %lf\n" % (curr_a, curr_b, curr_val))
  val.append(curr_val)
  a.append(curr_a)
  b.append(curr_b)
f.close()

# find mpv
index = argmin(val)
print "best estimates: a =", a[index], "b =", b[index]

# plot posterior
post = []
m = max(val)
for i in range(0,len(val)):
  post.append(exp(-10*val[i]/m))
# 2D plot
plt.scatter(a, b, c=post, s=200)
plt.xlabel('a')
plt.ylabel('b')
plt.colorbar()
plt.show()
# 3D plot
fig=p.figure()
ax = p3.Axes3D(fig)
ax.scatter(a, b, post, c=post, s=200)
ax.set_xlabel('a')
ax.set_ylabel('b')
ax.set_zlabel('frequency')
fig.add_axes(ax)
p.show()

