#include <iostream>
#include <cstdlib>

#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridReader.h>

int main ( int argc, char *argv[] )
{
  //parse command line arguments
  if(argc != 9)
  {
    std::cout << "Usage: " << argv[0]
              << " Filename(.vtu) dataname xmin xmax ymin ymax dx dy" << std::endl;
    return -1;
  }
  std::string filename = argv[1];
  std::string dataname = argv[2];
  double xmin = atof(argv[3]);
  double xmax = atof(argv[4]);
  double ymin = atof(argv[5]);
  double ymax = atof(argv[6]);
  double dx = atof(argv[7]);
  double dy = atof(argv[8]);

  //read all the data from the file
  vtkSmartPointer<vtkXMLUnstructuredGridReader> reader =
    vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
  if (!reader->CanReadFile(filename.c_str()))
  {
    std::cout << "Cannot read file \"" << filename << "\"." << std::endl;
    return -1;
  }
  reader->SetFileName(filename.c_str());
  reader->Update();

  // read unstructured grid
  vtkUnstructuredGrid *output = reader->GetOutput();
  if (!output)
  {
    std::cout << "Cannot get output." << std::endl;
    return -1;
  }

  // read cells
  vtkCellArray *cell_array = output->GetCells();
  if (!cell_array)
  {
    std::cout << "Cannot get cell array." << std::endl;
    return -1;
  }

  vtkPoints *points_array = output->GetPoints();
  if (!points_array)
  {
    std::cout << "Cannot get points array." << std::endl;
    return -1;
  }

  // read scalars field
  vtkCellData *cell_data = output->GetCellData();
  if (!cell_data)
  {
    std::cout << "Cannot get cell data." << std::endl;
    return -1;
  }
  vtkDataArray *scalars = cell_data->GetScalars(dataname.c_str());
  if (!scalars)
  {
    std::cout << "Cannot get scalars." << std::endl;
    return -1;
  }

  // check bounds
  double bounds[6];
  output->GetBounds(bounds);
  if (xmin > xmax)
  {
    std::cout << "Error: xmin(" << xmin << ") is greater than xmax(" << xmax << "). Abort." << std::endl;
    return -1;
  }
  if (xmin > bounds[1])
  {
    std::cout << "Error: xmin (" << xmin << ") is greater than the right bound (" << bounds[1] << "). Abort." << std::endl;
    return -1;
  }
  if (xmin < bounds[0])
  {
    std::cout << "Warning: xmin (" << xmin << ") is less than the left bound(" << bounds[1] << ");"
              << " set xmin equal to the left bound." << std::endl;
    xmin = bounds[0];
  }
  if (xmax < bounds[0])
  {
    std::cout << "Error: xmax (" << xmax << ") is less than the left bound (" << bounds[0] << "). Abort." << std::endl;
    return -1;
  }
  if (xmax > bounds[1])
  {
    std::cout << "Warning: xmax (" << xmax << ") is greater than the right bound (" << bounds[1] << ");"
              << " set xmax equal to the right bound." << std::endl;
    xmax = bounds[1];
  }
  if (ymin > bounds[3])
  {
    std::cout << "Error: ymin (" << ymin << ") is greater than the upper bound (" << bounds[3] << "). Abort." << std::endl;
    return -1;
  }
  if (ymin < bounds[2])
  {
    std::cout << "Warning: ymin (" << ymin << ") is less than the lower bound(" << bounds[1] << ");"
              << " set ymin equal to the lower bound." << std::endl;
    ymin = bounds[2];
  }
  if (ymax < bounds[2])
  {
    std::cout << "Error: ymax (" << ymax << ") is less than the lower bound (" << bounds[2] << "). Abort." << std::endl;
    return -1;
  }
  if (ymax> bounds[3])
  {
    std::cout << "Warning: ymax (" << ymax << ") is greater than the upper bound(" << bounds[1] << ");"
              << " set ymax equal to the upper bound." << std::endl;
    ymax = bounds[3];
  }
  if (dx > xmax-xmin)
  {
    std::cout << "Warning: dx (" << dx << ") is greater than the region width(" << xmax-xmin << ");"
              << " set dx equal to the region width." << std::endl;
    dx = xmax-xmin;
  }
  if (dy > ymax-ymin)
  {
    std::cout << "Warning: dy (" << dy << ") is greater than the region height(" << ymax-ymin << ");"
              << " set dy equal to the region height." << std::endl;
    dy = ymax-ymin;
  }
 
  // create an output file
  std::string resfile = filename.erase(filename.length()-4, 4)+".uniform";
  FILE *fp = fopen(resfile.c_str(), "w");
  if (!fp)
  {
    std::cout << "Cannot open file " << resfile << std::endl;
    return -1;
  }
  // write data to the file
  for (double y=ymax-0.5*dy; y>ymin; y-=dy)
    for (double x=xmin+0.5*dx; x<xmax; x+=dx)
    {
      double point_coord[3] = {x, y, 0.0};
      double pcoords[3];
      double weights[4];
      int subid;
      vtkIdType id = output->FindCell(point_coord, 0, 0, 0, subid, pcoords, weights);
      double value = scalars->GetTuple1(id);
      fprintf(fp, "%lf %lf %le\n", x, y, value);
    }
  fclose(fp);
  std::cout << "Output file \"" << resfile << "\" successfully created." << std::endl;

  return 0;
}

