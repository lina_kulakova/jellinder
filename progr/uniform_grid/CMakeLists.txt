
cmake_minimum_required(VERSION 2.8)

PROJECT(MakeUniformGrid)

find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

add_executable(MakeUniformGrid main)

if(VTK_LIBRARIES)
  target_link_libraries(MakeUniformGrid ${VTK_LIBRARIES})
else()
  target_link_libraries(MakeUniformGrid vtkHybrid)
endif()
