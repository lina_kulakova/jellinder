#!/usr/bin/python

from optparse import OptionParser
from string import atof
from math import sqrt
from os import listdir
from os.path import isfile, join
from modred import DMD
from numpy import dot, vdot, loadtxt, reshape
import matplotlib.pyplot as plt
from heapq import nlargest
import matplotlib.cm as cm
from pylab import *


# parse command line arguments
parser = OptionParser()
parser.add_option("-d", "--dir", dest="directory", help="Read files from the directory DIR", metavar="DIR")
(options, args) = parser.parse_args()


num_modes = 5

directory = options.directory
# read files
vecs = []
files = [ f for f in listdir(directory) if isfile(join(directory,f)) ]
for f in files:
  x,y,val = loadtxt(directory+"/"+f).T #Transposed for easier unpacking
  vecs.append(val)

# compute dmd
myDMD = DMD(vdot)
ritz_values, modes_norms, build_coeffs = myDMD.compute_decomp_in_memory(vecs)
# plot approximate eigenvalues
real = [k.real for k in ritz_values]
imag = [k.imag for k in ritz_values]
plt.plot(real,imag,'ro')
plt.show()
# choose the most unstable modes
largest_modes = sorted(range(len(real)), key=lambda i:real[i])
modes = myDMD.compute_modes_in_memory(largest_modes[0:num_modes])
# plot modes
lenx = 0
for i in range(0,len(x)):
  if y[i] == y[0]:
    lenx += 1
  else:
    break
ncols = lenx
nrows = len(y)/lenx
for num in range(0,num_modes):
  real = [k.real for k in modes[num]]
  imag = [k.imag for k in modes[num]]
  quiver(x,y,real,imag)
  show()

#print "Modes from directory \"", dir1, "\"", "successfully computed."

# compute norm
s = 0
for i in range(0,num_modes):
  for j in range(0,len(modes[i])):
    z = modes[i][j]
    s += z.real*z.real+z.imag*z.imag;
s = sqrt(s)
#print "DMD norm", s
print s

