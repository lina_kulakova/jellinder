#pragma once

#include "IF2D_Headers.h"
#include "IF2D_Types.h"
#include "IF2D_ObstacleOperator.h"
#include "IF2D_FloatingObstacleOperator.h"

class IF2D_Jellyfish: public IF2D_FloatingObstacleOperator
{
protected:
	double xm_corr, ym_corr, vx_corr, vy_corr, omega_corr;
public:
	class JellyfishCylinder  
	{
	private:
		Real _mollified_heaviside(const double dist, const double eps) const;
		
	public:		
		double angle, xm, ym, D, vx, vy, angular_velocity, m, J, rho, a, b;	
		
		JellyfishCylinder();
		JellyfishCylinder(Real xm, Real ym, Real D, Real angle_rad, Real a, Real b);	
		
		void update_all(double dt, double t);
		void restart(FILE * f);
		void save(FILE * f) const;
		Real sample(const Real x_, const Real y_, const Real eps) const;
                void sample(const Real ix, const Real iy, const double eps, Real & Xs, Real & defvelx, Real & defvely) const;
		void bbox(const Real eps, Real xmin[2], Real xmax[2]) const;
		void restart();
		void save();
	};
	
	JellyfishCylinder * shape;
	
        IF2D_Jellyfish(ArgumentParser & parser, Grid<W,B>& grid, const Real _xm, const Real _ym, const Real _D, const Real eps,
            const Real Uinf[2], IF2D_PenalizationOperator& penalization, const Real a_, const Real b_, const int LMAX = 0);
        ~IF2D_Jellyfish();
		
	void characteristic_function();		
	Real getD() const {return D;}
	
	void update(const double dt, const double t, string filename = std::string(), map< string,
            vector<IF2D_FloatingObstacleOperator *> > * _data = NULL);
	void computeDesiredVelocity(const double t);
	void create(const double t){}

	void save(const double t, string filename = std::string());
	void restart(const double t, string filename = std::string());
	void refresh(const double t, string filename = std::string());
	
protected:
	virtual void _setMotionPattern(const Real t);
};
