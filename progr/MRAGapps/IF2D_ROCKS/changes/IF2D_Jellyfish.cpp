#include "IF2D_Jellyfish.h"

IF2D_Jellyfish::JellyfishCylinder::JellyfishCylinder(): 
  angle(0), D(0.1), xm(0.5), ym(0.5), vx(0), vy(0), angular_velocity(0), rho(1.0), a(0.0), b(0.0)
{
	m = rho*M_PI*(0.5*D)*(0.5*D);
	J = 0.5*m*(0.5*D)*(0.5*D);
}

IF2D_Jellyfish::JellyfishCylinder::JellyfishCylinder(Real xm, Real ym, Real D, Real angle_rad, Real a, Real b):
  angle(angle_rad), D(D), xm(xm), ym(ym), vx(0), vy(0), angular_velocity(0), rho(1.0), a(a), b(b)
{
	m = rho*M_PI*(0.5*D)*(0.5*D);
	J = 0.5*m*(0.5*D)*(0.5*D);
}

Real IF2D_Jellyfish::JellyfishCylinder::_mollified_heaviside(const double dist, const double eps) const
{
	//Positive outside/negative inside
	const double alpha = M_PI*min(1., max(0., (dist+0.5*eps)/eps));			
	return 0.5+0.5*cos(alpha); // outside: 0, inside: 1, between: mollified heaviside
}

void IF2D_Jellyfish::JellyfishCylinder::update_all(double dt,double t)
{
	xm += vx*dt;
	ym += vy*dt;
	angle += angular_velocity*dt;
}

void IF2D_Jellyfish::JellyfishCylinder::restart(FILE * f)
{
	float val;

	if (fscanf(f, "xm: %e\n", &val) != 1) printf("Reading error detected!\n");
	xm = val;
	printf("JellyfishCylinder::restart(): xm is %e\n", xm);

	if (fscanf(f, "ym: %e\n", &val) != 1) printf("Reading error detected!\n");
	ym = val;
	printf("JellyfishCylinder::restart(): ym is %e\n", ym);

	if (fscanf(f, "vx: %e\n", &val) != 1) printf("Reading error detected!\n");
	vx = val;
	printf("JellyfishCylinder::restart(): vx is %e\n", vx);

	if (fscanf(f, "vy: %e\n", &val) != 1) printf("Reading error detected!\n");
	vy = val;
	printf("JellyfishCylinder::restart(): vy is %e\n", vy);

	if (fscanf(f, "a: %e\n", &val) != 1) printf("Reading error detected!\n");
	a = val;
	printf("JellyfishCylinder::restart(): a is %e\n", a);

	if (fscanf(f, "b: %e\n", &val) != 1) printf("Reading error detected!\n");
	b = val;
	printf("JellyfishCylinder::restart(): b is %e\n", b);

	if (fscanf(f, "angle: %e\n", &val) != 1) printf("Reading error detected!\n");
	angle = val;
	printf("JellyfishCylinder::restart(): angle is %e\n", angle);

	if (fscanf(f, "angular_velocity: %e\n", &val) != 1) printf("Reading error detected!\n");
	angular_velocity = val;
	printf("JellyfishCylinder::restart(): angular_velocity is %e\n", angular_velocity);
}

void IF2D_Jellyfish::JellyfishCylinder::save(FILE * f) const
{
	fprintf(f, "xm: %20.20e\n", xm);
	fprintf(f, "ym: %20.20e\n", ym);
	fprintf(f, "vx: %20.20e\n", vx);
	fprintf(f, "vy: %20.20e\n", vy);
	fprintf(f, "a: %20.20e\n", a);
	fprintf(f, "b: %20.20e\n", b);
	fprintf(f, "angle: %20.20e\n", angle);
	fprintf(f, "angular_velocity: %20.20e\n", angular_velocity);
}

Real IF2D_Jellyfish::JellyfishCylinder::sample(const Real x_, const Real y_, const Real eps) const
{
	const double dist = sqrt( (x_-xm)*(x_-xm) + (y_-ym)*(y_-ym) ) - 0.5*D;	// negative = inside cylinder, positive = outside cylinder
	return _mollified_heaviside(dist, eps);
}


static inline
Real dot_prod(const Real x1, const Real y1, const Real x2, const Real y2)
{
  return x1*x2+y1*y2;
}

#define ARBITRARY_E 0
void IF2D_Jellyfish::JellyfishCylinder::sample(const Real ix, const Real iy, const double eps, Real & Xs, Real & defvelx, Real & defvely) const
{
        // e -- swimming direction vecor
#if ARBITRARY_E
        Real ex = 0.0, ey = 1.0;
        Real enorm = sqrt(dot_prod(ex,ey,ex,ey));
#endif
        // r -- surface radius vector
        Real Rx = ix-xm, Ry = iy-ym;
        Real Rnorm = sqrt(dot_prod(Rx,Ry,Rx,Ry));

        // compute deformation velocity
#if ARBITRARY_E
        Real cos_theta = dot_prod(ex,ey,Rx,Ry) / (enorm*Rnorm);
        Real sin_theta = sqrt(1.0 - cos_theta*cos_theta);
#else
        Real cos_theta = Ry / Rnorm;
        Real sin_theta = Rx / Rnorm;
#endif
        Real defvel = fabs(b*sin_theta*(1.0 + a*cos_theta)); // velocity absolute value
//        Real defvel = b*sin_theta*(1.0 + a*cos_theta); // velocity absolute value
        Real mult = 2.0*Rnorm/D;
        mult *= mult;
        defvel *= mult;

        // compute velocity components
#if ARBITRARY_E
        if ((ix-xm)*ey-(iy-ym)*ex>0)
#else
        if (ix > xm)
#endif
        {
          defvelx = -defvel * cos_theta;
          defvely = defvel * sin_theta;
        }
#if ARBITRARY_E
        else if ((ix-xm)*ey-(iy-ym)*ex<0)
#else
        else if (ix < xm)
#endif
        {
          defvelx = defvel * cos_theta;
          defvely = -defvel * sin_theta;
        }
        else
        {
          defvelx = 0.0;
          defvely = 0.0;
        }


        // TEST
//        defvelx = (-iy+ym)*mult*defvel/Rnorm;
//        defvely = fabs(ix-xm)*mult*defvel/Rnorm;

        // compute characteristic function
	const Real dist = Rnorm - 0.5*D;	// negative = inside cylinder, positive = outside cylinder
	Xs = _mollified_heaviside(dist, eps);
}


 // calculate coordinates of a box around the cylinder
void IF2D_Jellyfish::JellyfishCylinder::bbox(const Real eps, Real xmin[2], Real xmax[2]) const
{
	assert(eps>=0);

	xmin[0] = xm - 0.5*D;
	xmin[1] = ym - 0.5*D;
	xmax[0] = xm + 0.5*D;
	xmax[1] = ym + 0.5*D;

	xmin[0] -= 2*eps;
	xmin[1] -= 2*eps;
	xmax[0] += 2*eps;
	xmax[1] += 2*eps;

	assert(xmin[0]<=xmax[0]);
	assert(xmin[1]<=xmax[1]);
}


void IF2D_Jellyfish::JellyfishCylinder::restart()
{
	//FILE *f = fopen("rotatingwheel.restart", "r");
	//for(int i=0; i<shapes.size(); i++)
	//	shapes[i]->restart(f);
	//fclose(f);
}

void IF2D_Jellyfish::JellyfishCylinder::save()
{
	//FILE * f = fopen("rotatingwheel.restart", "w");
	//for(int i=0; i<shapes.size(); i++)
	//	shapes[i]->save(f);
	//fclose(f);
}


namespace Jellyfish
{
struct FillBlocks
{
	Real eps;
	IF2D_Jellyfish::JellyfishCylinder * shape;

	FillBlocks(Real eps, IF2D_Jellyfish::JellyfishCylinder *_shape): eps(eps) { shape = _shape; }

	FillBlocks(const FillBlocks& c): eps(c.eps) { shape = c.shape; }

        // true if cylinder coincides with block
	static bool _is_touching(Real eps, const IF2D_Jellyfish::JellyfishCylinder * wheel, const BlockInfo& info)
	{
		Real min_pos[2], max_pos[2];

		info.pos(min_pos, 0,0);
		info.pos(max_pos, FluidBlock2D::sizeX-1, FluidBlock2D::sizeY-1);

		Real bbox[2][2];
		wheel->bbox(eps, bbox[0], bbox[1]);

		Real intersection[2][2] = {
				max(min_pos[0], bbox[0][0]), min(max_pos[0], bbox[1][0]),
				max(min_pos[1], bbox[0][1]), min(max_pos[1], bbox[1][1]),
		};

		return
				intersection[0][1]-intersection[0][0]>0 &&
				intersection[1][1]-intersection[1][0]>0 ;
	}

	inline void operator()(const BlockInfo& info, FluidBlock2D& b) const
	{
		bool bEmpty = true;

		if(_is_touching(eps, shape, info))
		{
			for(int iy=0; iy<FluidBlock2D::sizeY; iy++)
				for(int ix=0; ix<FluidBlock2D::sizeX; ix++)
				{
					Real p[2];
					info.pos(p, ix, iy); // returns physical coordinates of each point within the box

					b(ix,iy).tmp = max( shape->sample(p[0], p[1], eps), b(ix,iy).tmp );
				}

			bEmpty = false;
		}
	}
};


struct GetNonEmpty
{
	Real eps;
	IF2D_Jellyfish::JellyfishCylinder * shape;
	map<int, bool>& b2nonempty;

	GetNonEmpty(Real eps, IF2D_Jellyfish::JellyfishCylinder *_shape, map<int, bool>& b2nonempty): eps(eps), b2nonempty(b2nonempty)
	{
		shape = _shape;
	}

	GetNonEmpty(const GetNonEmpty& c): eps(c.eps), b2nonempty(c.b2nonempty)
	{
		shape = c.shape;
	}

	inline void operator()(const BlockInfo& info, FluidBlock2D& b) const
	{
		bool bNonEmpty = false;

		if(FillBlocks::_is_touching(eps, shape, info))
		{
			for(int iy=0; iy<FluidBlock2D::sizeY; iy++)
				for(int ix=0; ix<FluidBlock2D::sizeX; ix++)
				{
					Real p[2];
					info.pos(p, ix, iy);

					const Real Xs = shape->sample(p[0], p[1], eps);
					bNonEmpty |= Xs>0;
				}

			assert(b2nonempty.find(info.blockID) != b2nonempty.end());

			b2nonempty[info.blockID] = bNonEmpty;
		}
	}
};


struct ComputeAll
{
	Real Uinf[2];
	double eps, xcm, ycm;
	IF2D_Jellyfish::JellyfishCylinder * shape;
	map<int, vector<double> >& b2sum;
	map<int, bool>& b2nonempty;

	ComputeAll(double xcm, double ycm, double eps, IF2D_Jellyfish::JellyfishCylinder *_shape, map<int, vector<double> >& b2sum,
            Real Uinf[2], map<int, bool>& b2nonempty): xcm(xcm), ycm(ycm), eps(eps), b2sum(b2sum), b2nonempty(b2nonempty)
	{
		this->Uinf[0] = Uinf[0];
		this->Uinf[1] = Uinf[1];

		shape = _shape;
	}

	ComputeAll(const ComputeAll& c): xcm(c.xcm), ycm(c.ycm), eps(c.eps), b2sum(c.b2sum), b2nonempty(c.b2nonempty)
	{
		Uinf[0] = c.Uinf[0];
		Uinf[1] = c.Uinf[1];

		shape = c.shape;
	}

	inline void operator()(const BlockInfo& info, FluidBlock2D& b) const
	{
		bool bNonEmpty = false;

		if(FillBlocks::_is_touching(eps, shape, info))
		{
			double mass = 0;
			double J = 0;
			double vxbar = 0;
			double vybar = 0;
			double omegabar = 0;

			for(int iy=0; iy<FluidBlock2D::sizeY; iy++)
				for(int ix=0; ix<FluidBlock2D::sizeX; ix++)
				{
					double p[2];
					info.pos(p, ix, iy);

					const double Xs = shape->sample(p[0], p[1], eps);
					bNonEmpty |= Xs>0;

					mass += Xs;
					J += Xs*((p[0]-xcm)*(p[0]-xcm) + (p[1]-ycm)*(p[1]-ycm));
					vxbar += Xs*(b(ix, iy).u[0]+Uinf[0]);
					vybar += Xs*(b(ix, iy).u[1]+Uinf[1]);
					omegabar += Xs*((b(ix, iy).u[1]+Uinf[1])*(p[0]-xcm)-(b(ix, iy).u[0]+Uinf[0])*(p[1]-ycm));
				}

			assert(b2sum.find(info.blockID) != b2sum.end());
			assert(b2nonempty.find(info.blockID) != b2nonempty.end());

			b2sum[info.blockID][0] = mass*info.h[0]*info.h[0];
			b2sum[info.blockID][1] = J*info.h[0]*info.h[0];
			b2sum[info.blockID][2] = vxbar*info.h[0]*info.h[0];
			b2sum[info.blockID][3] = vybar*info.h[0]*info.h[0];
			b2sum[info.blockID][4] = omegabar*info.h[0]*info.h[0];
			b2nonempty[info.blockID] = bNonEmpty;
		}
	}
};


struct FillVelblocks
{
        double eps, xcm, ycm, vxcorr, vycorr, omegacorr;
	IF2D_Jellyfish::JellyfishCylinder * shape;
	vector<pair< BlockInfo, VelocityBlock *> >& workitems;

	FillVelblocks(vector<pair< BlockInfo, VelocityBlock *> >& workitems, double xcm, double ycm, double vxcorr, double vycorr,
            double omegacorr, double eps, IF2D_Jellyfish::JellyfishCylinder *_shape):
		workitems(workitems), xcm(xcm), ycm(ycm), vxcorr(vxcorr), vycorr(vycorr), omegacorr(omegacorr), eps(eps)
	{
		shape = _shape;
	}

	FillVelblocks(const FillVelblocks& c): workitems(c.workitems), xcm(c.xcm), ycm(c.ycm), vxcorr(c.vxcorr), vycorr(c.vycorr),
        omegacorr(c.omegacorr), eps(c.eps)
	{
		shape = c.shape;
	}

	void operator()(blocked_range<int> range) const
	{
		for(int iblock=range.begin(); iblock<range.end(); iblock++)
		{
			BlockInfo info = workitems[iblock].first;
			VelocityBlock * u_desired = workitems[iblock].second;

			const Real xm = shape->xm;
			const Real ym =	shape->ym;
			const double av = shape->angular_velocity - omegacorr;
			const double vx = shape->vx - vxcorr;
			const double vy = shape->vy - vycorr;

			if(FillBlocks::_is_touching(eps, shape, info))
				for(int iy=0; iy<FluidBlock2D::sizeY; iy++)
					for(int ix=0; ix<FluidBlock2D::sizeX; ix++)
					{
						Real p[2];
						info.pos(p, ix, iy);

						Real Xs, defvelx, defvely;
                                                shape->sample(p[0], p[1], eps, Xs, defvelx, defvely);

						u_desired->u[0][iy][ix] = (Xs>0)?(- av*(p[1]-ym) + defvelx + vx):0.0;
						u_desired->u[1][iy][ix] = (Xs>0)?(+ av*(p[0]-xm) + defvely + vy):0.0;
					}
		}
	}
};
}


IF2D_Jellyfish::IF2D_Jellyfish(ArgumentParser & parser, Grid<W,B>& grid, const Real _xm, const Real _ym,
    const Real _D, const Real eps, const Real Uinf[2], IF2D_PenalizationOperator& penalization, const Real a_, const Real b_, const int LMAX):
  IF2D_FloatingObstacleOperator(parser, grid, D, eps, Uinf, penalization), xm_corr(0.0), ym_corr(0.0), vx_corr(0.0), vy_corr(0.0), omega_corr(0.0)
{
	this->Uinf[0] = Uinf[0];
	this->Uinf[1] = Uinf[1];

	shape = new JellyfishCylinder(_xm, _ym, _D, 0.0, a_, b_);
}

IF2D_Jellyfish::~IF2D_Jellyfish()
{
	assert(shape!=NULL);
	if(shape!=NULL)
	{
		delete shape;
		shape = NULL;
	}
}


void IF2D_Jellyfish::characteristic_function()
{
	vector<BlockInfo> vInfo = grid.getBlocksInfo();
	const BlockCollection<B>& coll = grid.getBlockCollection();

	Jellyfish::FillBlocks fill(eps,shape);
	block_processing.process(vInfo, coll, fill);
}

void IF2D_Jellyfish::restart(const double t, string filename)
{
	FILE * ppFile = NULL;

	if(filename==std::string())
	{
		// If the string is not set I write my own file
		ppFile = fopen("restart_IF2D_Jellyfish.txt", "r");
		assert(ppFile!=NULL);
	}
	else
	{
		// If string is set I open the corresponding file
		ppFile = fopen(filename.c_str(), "r");
		assert(ppFile!=NULL);
	}

	// Actual restart
	shape->restart(ppFile);

	// Cloase file
	fclose(ppFile);
}

void IF2D_Jellyfish::save(const double t, string filename)
{
	FILE * ppFile = NULL;

	if(filename==std::string())
	{
		// If the string is not set I write my own file
		ppFile = fopen("restart_IF2D_Jellyfish.txt", "w");
		assert(ppFile!=NULL);
	}
	else
	{
		// If string is set I open the corresponding file
		ppFile = fopen(filename.c_str(), "w");
		assert(ppFile!=NULL);
	}

	// Actual restart
	shape->save(ppFile);

	// Cloase file
	fclose(ppFile);
}

void IF2D_Jellyfish::refresh(const double t, string filename)
{
	// Open file stream
	ifstream filestream;
	if(filename==std::string())
	{
		// If the string is not set I write my own file
		filestream.open("restart_IF2D_Jellyfish.txt");
		if (!filestream.good())
		{
			cout << "ooops: file not found. Exiting now." << endl;
			exit(-1);
		}
	}
	else
	{
		filestream.open(filename.c_str());
		if (!filestream.good())
		{
			cout << "ooops: file not found. Exiting now." << endl;
			exit(-1);
		}
	}

	// Open file
	int c = 0;
	string line;
	while( getline(filestream, line) ) c++;
	filestream.close();

	FILE * f = NULL;
	if(filename==std::string())
	{
		// If the string is not set I write my own file
		f = fopen("restart_IF2D_Jellyfish.txt", "r");
		assert(f!=NULL);
	}
	else
	{
		// If string is set I open the corresponding file
		f = fopen(filename.c_str(), "r");
		assert(f!=NULL);
	}

	//data stored in dataVect until t=t_restart
	// TODO: make it automatic!
	vector < vector<float> > dataVect;
	for(int i=0; i<c; i++)
	{
		vector<float> row;
		float variable = 0.0;
		if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
		const Real dimensionlessTime = variable;
		if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
		const Real time = variable;
		if(time <= t)
		{
			row.push_back(dimensionlessTime);
			row.push_back(time);
			if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real xm = variable;
			row.push_back(xm);
			if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real ym = variable;
			row.push_back(ym);
			if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real vx = variable;
			row.push_back(vx);
			if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real vy = variable;
			row.push_back(vy);
                        if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real a = variable;
			row.push_back(a);
                        if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real b = variable;
			row.push_back(b);
			if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real angle = variable;
			row.push_back(angle);
			if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real angular_velocity = variable;
			row.push_back(angular_velocity);
			if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real J = variable;
			row.push_back(J);
			if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real m = variable;
			row.push_back(m);
			if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real rho = variable;
			row.push_back(rho);
			if (fscanf(f,"%f",&variable) != 1) printf("Reading error detected!\n");
			const Real cD = variable;
			row.push_back(cD);
		}
		else
		{
			break;
		}
		dataVect.push_back(row);
	}
	fclose(f);
	f = NULL;

	//dataVect is copied in a new "shape_001" file
	if(filename==std::string())
	{
		// If the string is not set I write my own file
		f = fopen("restart_IF2D_Jellyfish.txt", "w");
		assert(f!=NULL);
	}
	else
	{
		// If string is set I open the corresponding file
		f = fopen(filename.c_str(), "w");
		assert(f!=NULL);
	}

	// Print stuff before t<t_restart
	for(unsigned int i=0; i<dataVect.size(); i++)
	{
		for(unsigned int j=0; j<dataVect[i].size(); j++)
			fprintf(f, "%e ", dataVect[i][j]);

		fprintf(f, "\n");
	}

	fclose(f);
}

void IF2D_Jellyfish::_setMotionPattern(const Real t)
{
        shape->vx = 0.0;
//      shape->vy = 0.0;
//	shape->angular_velocity = 0.0;
}


void IF2D_Jellyfish::computeDesiredVelocity(const double t)
{
//       _setMotionPattern(t);
	const int NQUANTITIES = 5;

	double mass = 0.0;
	double J = 0.0;
	double xbar = 0.0;
	double ybar = 0.0;
	double vxbar = 0.0;
	double vybar = 0.0;
	double omegabar = 0.0;

	vector<BlockInfo> vInfo = grid.getBlocksInfo();
	const BlockCollection<B>& coll = grid.getBlockCollection();

	map<int, vector<double> > integrals;
	for(vector<BlockInfo>::const_iterator it=vInfo.begin(); it!=vInfo.end(); it++)
		integrals[it->blockID] = vector<double>(NQUANTITIES);

	map<int, bool> nonempty;
	for(vector<BlockInfo>::const_iterator it=vInfo.begin(); it!=vInfo.end(); it++)
		nonempty[it->blockID] = false;

	// Compute all from the flow
	mass = J = xbar = ybar = vxbar = vybar = omegabar = 0.0;
	Jellyfish::ComputeAll computeAll(xm_corr, ym_corr, eps, shape, integrals, Uinf, nonempty);
	block_processing.process(vInfo, coll, computeAll);
	for(map<int, vector< double> >::const_iterator it= integrals.begin(); it!=integrals.end(); ++it)
	{
		mass += (it->second)[0];
		J += (it->second)[1];
		vxbar += (it->second)[2];
		vybar += (it->second)[3];
		omegabar += (it->second)[4];			
	}
	vxbar /= mass;
	vybar /= mass;
	omegabar /= J;

	// Set the right vx, vy and angular velocity
	shape->vx = vxbar;
	shape->vy = vybar;
	shape->angular_velocity = omegabar;
	shape->m = mass;
	shape->J = J;

	// Prepare desired velocity blocks
	for	(map<int, const VelocityBlock *>::iterator it = desired_velocity.begin(); it!= desired_velocity.end(); it++)
	{
		assert(it->second != NULL);
		VelocityBlock::deallocate(it->second);
	}
	desired_velocity.clear();
	vector<pair< BlockInfo, VelocityBlock *> > velblocks;
	for(vector<BlockInfo>::const_iterator it=vInfo.begin(); it!=vInfo.end(); it++)
	{
		if(nonempty[it->blockID] == true)
		{
			VelocityBlock * velblock = VelocityBlock::allocate(1);
			desired_velocity[it->blockID] = velblock;
			velblocks.push_back(pair< BlockInfo, VelocityBlock *>(*it, velblock));
		}
	}

	// Set desired velocities
	Jellyfish::FillVelblocks fillvelblocks(velblocks, xm_corr, ym_corr, vx_corr, vy_corr, omega_corr, eps, shape);
	tbb::parallel_for(blocked_range<int>(0, velblocks.size()), fillvelblocks, auto_partitioner());



//	_setMotionPattern(t);
//
//	vector<BlockInfo> vInfo = grid.getBlocksInfo();
//	const BlockCollection<B>& coll = grid.getBlockCollection();
//
//	map<int, vector<double> > integrals;
//	for(vector<BlockInfo>::const_iterator it=vInfo.begin(); it!=vInfo.end(); it++)
//		integrals[it->blockID] = vector<double>(1);
//
//	map<int, bool> nonempty;
//	for(vector<BlockInfo>::const_iterator it=vInfo.begin(); it!=vInfo.end(); it++)
//		nonempty[it->blockID] = false;
//
//	// Get non-empty blocks
//	Jellyfish::GetNonEmpty getNonEmpty(eps, shape, nonempty);
//	block_processing.process(vInfo, coll, getNonEmpty);
//
//	// Set desired velocities
//	for	(map<int, const VelocityBlock *>::iterator it = desired_velocity.begin(); it!= desired_velocity.end(); it++)
//	{
//		assert(it->second != NULL);
//		VelocityBlock::deallocate(it->second);
//		it->second = NULL;
//	}	
//	desired_velocity.clear();
//
//	vector<pair< BlockInfo, VelocityBlock *> > velblocks;
//	for(vector<BlockInfo>::const_iterator it=vInfo.begin(); it!=vInfo.end(); it++)
//	{
//		if(nonempty[it->blockID] == true)
//		{
//			VelocityBlock * velblock = VelocityBlock::allocate(1);
//			desired_velocity[it->blockID] = velblock;
//			velblocks.push_back(pair< BlockInfo, VelocityBlock *>(*it, velblock));
//		}
//	}
//
//	Jellyfish::FillVelblocks fillvelblocks(velblocks, eps, shape);
//	tbb::parallel_for(blocked_range<int>(0, velblocks.size()), fillvelblocks, auto_partitioner());
}

void IF2D_Jellyfish::update(const double dt, const double t, string filename, map< string, vector<IF2D_FloatingObstacleOperator *> > * _data)
{
	shape->update_all(dt,t);

	FILE * ppFile = NULL;

	if(filename==std::string())
	{
		// If the string is not set I write my own file
		ppFile = fopen("update_IF2D_Jellyfish.txt", t == 0.0 ? "w" : "a");
		assert(ppFile!=NULL);		
	}
	else
	{
		// If string is set I open the corresponding file
		ppFile = fopen(filename.c_str(), t == 0.0 ? "w" : "a");
		assert(ppFile!=NULL);
	}

	// Write update data
	fprintf(ppFile, "%e %e %e %e %e %e %e %e %e %e %e %e %e %e\n", this->dimT, t, shape->xm, shape->ym, shape->vx, shape->vy,
            shape->a, shape->b, shape->angle, shape->angular_velocity, shape->J, shape->m, shape->rho, this->Cd);
	fflush(ppFile);

	// Cloase file
	fclose(ppFile);
}

